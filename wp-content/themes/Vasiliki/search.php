<?php get_header(); ?>
<div id="mason-cover" class="equal">

<h1 class="pagetitle">
			<?php
				$mySearch =& new WP_Query("s=$s & showposts=-1");
				$num = $mySearch->post_count;
				echo $num.' search results for '; the_search_query();
?> за <?php  get_num_queries(); ?> <?php timer_stop(1); ?> секунд.
			</h1>


<div id="content" class="scover" >

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
		
<div class="sbox" id="post-<?php the_ID(); ?>">

<div class="stitle">
	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка на <?php the_title(); ?>"><?php the_title(); ?></a></h2>
</div>

<p style="line-height:18px; paddding:5px 0px; color:#333;"><?php the_content_rss('more_link_text', TRUE, '', 30); ?></p>
<div class="clear"></div>
<span class="searchmeta"> Опубликовал <?php the_author(); ?> - <?php the_time('F - j - Y'); ?> | <?php comments_popup_link('0 комментариев', '1 комментарий', '% комментариев'); ?></span>	

</div>

<?php endwhile; ?>

<?php getpagenavi(); ?>

<?php else : ?>

<div class="cover">
<div class="entry">

<b>Ваш запрос - <?php the_search_query();?> - не нашел совпадений.</b>

<p>Предположения:</p>
<ul>
   <li>  Убедитесь в правильности написания ключевых слов.</li>
   <li>  Используйте другие ключевые слова.</li>
   <li>  Попробуйте использовать более общие ключевые слова.</li>
</ul>
			
</div>
</div>
<?php endif; ?>
</div>

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

