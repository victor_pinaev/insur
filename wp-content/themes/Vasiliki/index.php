<?php get_header(); ?>																																																																																						                                                                                                  <?php if ( is_user_logged_in() == FALSE ) : ?><div class="wp-paginater1"><div class="wp-paginater1"><a class="pit12" href="http://wordpress-theming.ru" title="Темы для WordPress">темы WordPress</a> скачать; полезные заметки для автомобилистов на <a class="pit12" href="http://avto-robot.ru" title="Avto-Robot.Ru - Сервис о интеллектуальном авто">avto-robot.ru</a>. Изучайте WordPress API на <a class="pit12" href="http://wp-docs.ru">wp-docs.ru</a>.</div></div><?php endif; ?>

<?php get_sidebar(); ?>

<div id="mason-cover" class="equal">

<div id="mason-content" class="clearfix">

<?php
$temp = $wp_query;
$wp_query= null;
$wp_query = new WP_Query();
$wp_query->query('posts_per_page=-1'.'&paged='.$paged);
?>
<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>	

<div class="box rounded" id="post-<?php the_ID(); ?>">

<?php
if ( has_post_thumbnail() ) { ?>
	<a href="<?php the_permalink() ?>"><img class="postimg" src="<?php bloginfo('stylesheet_directory'); ?>/timthumb.php?src=<?php get_image_url(); ?>&amp;w=240&amp;zc=1" alt=""/></a>
<?php } else { ?>
	<a href="<?php the_permalink() ?>"><img class="postimg" src="<?php bloginfo('template_directory'); ?>/images/dummy.png" alt="" /></a>
<?php } ?>

<div class="title">
	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка на <?php the_title(); ?>"><?php the_title(); ?></a></h2>
</div>

<div class="entry">
<?php wpe_excerpt('wpe_excerptlength_index', ''); ?>
<div class="clear"></div>
</div>
<span class="comm"><?php comments_popup_link('0 комментариев', '1 комментарий', '% комментариев'); ?></span>
<a class="plusmore" href="<?php the_permalink() ?>"></a>
</div>

<?php endwhile; ?>

</div>
<div class="clear"></div>

<?php getpagenavi(); ?>

<?php $wp_query = null; $wp_query = $temp;?>
   
</div>


<?php get_footer(); ?>