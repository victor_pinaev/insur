jQuery(document).ready(function() {

/* Navigation */
jQuery('#submenu ul.sfmenu').superfish({ 
		delay:       500,								// 0.1 second delay on mouseout 
		animation:   {opacity:'show',height:'show'},	// fade-in and slide-down animation 
		dropShadows: true								// disable drop shadows 
	});	


/* Masonry*/
var jQuerycontainer = jQuery('#mason-content');
jQuerycontainer.imagesLoaded( function(){
  jQuerycontainer.masonry({
  itemSelector : '.box',
  isAnimated: true
  });
  var maxHeight = 0;
jQuery(".equal").each(function(){
   if (jQuery(this).height() > maxHeight) { maxHeight = jQuery(this).height(); }
});
jQuery(".equal").height(maxHeight); 
});


	
jQuery('.squarebanner ul li:nth-child(even)').addClass('rbanner');

});