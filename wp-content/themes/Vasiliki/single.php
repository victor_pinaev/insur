<?php get_header(); ?>
<div id="mason-cover" class="equal">
<div id="content" >

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

<div class="post" id="post-<?php the_ID(); ?>">

<div class="title">
<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка на <?php the_title(); ?>"><?php the_title(); ?></a></h2>
</div>
<div class="postmeta">
	<div class="pmet">Опубликовал <?php the_author(); ?> - <?php the_time('M - j - Y'); ?></div>
	
</div>	
<div class="cover">
<div class="entry">
<?php the_content('Читать далее &raquo;'); ?>

<div class="clear"></div>
<?php wp_link_pages(array('before' => '<p><strong>Страницы: </strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
</div>
</div>

<div class="singleinfo">
<span class="category">Рубрики: <?php the_category(', '); ?> </span>
</div>

</div>

<?php comments_template(); ?>
<?php endwhile; else: ?>

		<h1 class="title">Не найдено</h1>
		<p>Извините, но по Вашему запросу ничего не было найдено. Попробуйте изменить ключевые слова.</p>

<?php endif; ?>

</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>