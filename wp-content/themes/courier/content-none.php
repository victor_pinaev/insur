<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * @package Theme Meme
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title">Не найдено</h1>
	<!-- .page-header --></header>

	<div class="clearfix page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( 'Готовы опубликовать свой первый пост? <a href="%1$s">Вперед</a>.', esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p>К сожалению ничего не найдено. Попробуйте поискать по другим ключевым словам.</p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p>Кажется, мы не можем найти то, что вы ищете. Возможно, поиск может помочь.</p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	<!-- .page-content --></div>
<!-- .no-results --></section>