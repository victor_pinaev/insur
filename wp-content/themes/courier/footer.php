<?php
/**
 * The template for displaying the footer.
 *
 * @package Theme Meme
 */
?>
		</div>
	<!-- .site-main --></div>
                
	<footer class="site-footer" role="contentinfo">
		<div class="clearfix container">
				<div class="row">
					<div class="col-sm-6 site-info">
						&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.
					<!-- .site-info --></div>

					<div class="col-sm-6 site-credit">
						<?php __wordpress_pf(); ?>
					<!-- .site-credit --></div>
				</div>
		</div>
	<!-- .site-footer --></footer>

<?php wp_footer(); ?>
<!-- jQuery library (served from Google) -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/bxslider/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
    
    $('.bxslider').bxSlider({
            auto: true,
            controls: false,
            adaptiveHeight: true,
        });
    
});
</script>
</body>
</html>