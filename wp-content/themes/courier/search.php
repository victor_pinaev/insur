<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Theme Meme
 */

get_header(); ?>

	<div class="row">

		<?php if ( have_posts() ) : ?>

		<div class="col-xs-12 content-area" role="main">

			<header class="page-header">
				<h1 class="page-title"><?php printf( 'Результаты поиска для: %s', '<span>' . get_search_query() . '</span>' ); ?></h1>
			<!-- .page-header --></header>

			<div class="row items">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'search' ); ?>

			<?php endwhile; ?>
			</div>

			<?php themememe_content_nav( 'nav-below' ); ?>

		<!-- .content-area --></div>

		<?php else : ?>

		<div class="col-md-8 col-md-offset-2 content-area" role="main">

			<?php get_template_part( 'content', 'none' ); ?>

		<!-- .content-area --></div>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>