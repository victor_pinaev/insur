<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Theme Meme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title( '-', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
                <!-- bxSlider CSS file -->
                <link href="<?php bloginfo( 'template_url' ); ?>/bxslider/jquery.bxslider.css" rel="stylesheet" />    

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
                <div class="site-top">
		<div class="clearfix container">
			<nav class="site-menu" role="navigation">
				<div class="menu-toggle"><i class="fa fa-bars"></i></div>
				<div class="menu-text"></div>
				<?php wp_nav_menu( array( 'container_class' => 'clearfix menu-bar', 'theme_location' => 'primary',
                                    'menu' => 'menu') ); ?>
			<!-- .site-menu --></nav>
                            </div>
	<!-- .site-top --></div>
	<header class="site-header" role="banner">
		<div class="clearfix container">
			<div class="site-branding">
                            <a href="#"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png"</a>
			</div>
		</div>
                              
	<!-- .site-header --></header>
                
	<div class="site-main">
		<div class="clearfix container">
                              
