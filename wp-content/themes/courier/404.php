<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Theme Meme
 */

get_header(); ?>

	<div class="row">
		<div class="col-md-8 col-md-offset-2 content-area" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">Не найдено</h1>
				<!-- .page-header --></header>

				<div class="page-content">
					<p>К сожалению ничего не найдено. Попробуйте воспользоваться поиском.</p>

					<?php get_search_form(); ?>

					<?php the_widget( 'WP_Widget_Recent_Posts', '' , 'before_title=<h3 class="widget-title">&after_title=</h3>' ); ?>

					<?php if ( themememe_categorized_blog() ) : ?>
					<div class="widget widget_categories">
						<h3 class="widget-title">Наиболее популярные рубрики</h3>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div>
					<?php endif; ?>

					<?php
					$archive_content = '<p>' . sprintf( 'Попробуйте посмотреть ежемесячные архивы. %1$s', convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', 'before_title=<h3 class="widget-title">&after_title=</h3>' );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud', '' , 'before_title=<h3 class="widget-title">&after_title=</h3>' ); ?>

				<!-- .page-content --></div>
			<!-- .error-404 --></section>

		<!-- .content-area --></div>
	</div>

<?php get_footer(); ?>