<?php
/**
 * The template part for displaying content.
 *
 * @package Theme Meme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-sm-6 col-md-4 col-lg-3 item'); ?>>
	<?php if ( has_post_thumbnail() ): ?>
	<div class="entry-thumbnail">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail( 'thumb-medium' ); ?>
		</a>
	</div>
	<?php endif; ?>

	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php themememe_posted_on(); ?>

			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
			<span class="comments-link">
				<i class="fa fa-comments"></i>
				<?php comments_popup_link( '0 коммент.', '1 коммент.', '% коммент.' ); ?>
			</span>
			<?php endif; ?>
		<!-- .entry-meta --></div>
		<?php endif; ?>
	<!-- .entry-header --></header>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	<!-- .entry-summary --></div>
<!-- #post-<?php the_ID(); ?> --></article>