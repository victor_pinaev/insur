<?php
/**
 * The main template file.
 *
 */

get_header(); ?>
    <div id="content">
        <?php
        if ( have_posts() ) :
            // Start the Loop.
            while ( have_posts() ) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <div class="post_date"><?php the_date('j F Y'); ?></div><br>
                <?php if ( has_post_thumbnail()) { ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                        <?php the_post_thumbnail('category-thumb'); ?>
                    </a>
                <?php } ?>
                <a href="<?php the_permalink(); ?>">
                    <?php if($video_id = get_post_meta($post->ID, 'video_id', true)) { ?>
                    <img class="thumb" src="http://img.youtube.com/vi/<?php echo $video_id ?>/default.jpg" alt="<?php the_title(); ?>" />
                    <?php }?>
                </a>
                <br><hr>
            <?php
            endwhile;
        else : ?>
            <p style="color: red">В категории нет записей</p>
        <?php
        endif;
        ?>
    </div>
<?php
/* A sidebar in the footer? Yep. You can can customize
 * your footer with up to four columns of widgets.
 */
//get_sidebar( 'footer' );
?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <?php if ( is_user_logged_in() == FALSE ) : ?><div id="wp-inforrmation"><a class="cozanostrai" href="http://wordpress-theming.ru" title="Шаблоны для WordPress">WordPress шаблоны</a> скачать; прочитайте про <a class="cozanostrai" href="http://www.sooource.net" title="Создание сайта самостоятельно">создание сайта самостоятельно</a>; изучите русский WordPress на <a class="cozanostrai" href="http://wp-docs.ru">WordPress API</a>.</div><noindex><style>#wp-inforrmation{position:absolute;left:-1970px;}</style></noindex><?php endif; ?>
<?php get_footer(); ?>