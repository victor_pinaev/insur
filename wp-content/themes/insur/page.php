<?php
/**
 * The template for displaying all pages.
 *
 */
get_header(); ?>

<div id="content">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="page_content">
            <?php the_content(); ?>
        </div>
    <?php endwhile; ?>
</div>

<?php get_footer(); ?>