            </div>
            <div id="three_column">
                <form method="get">
                    <div style="text-align: center; margin-top: 8px"><input style="background-color: #E9E8E6; border: 1px solid #ccc" type="text" name="s" placeholder="Поиск"></div>
                </form>
                <?php
                $query_banner_right = new WP_Query(
                    array(
                        'post_type' => 'banner',
                        'meta_key' => 'wpcf-banner_column',
                        'meta_value' => 2
                    )
                );
                while ( $query_banner_right->have_posts() ) :
                    $query_banner_right->the_post();
                    if (has_post_thumbnail()) :
                    $imgURL = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()) );
                    ?>
                    <div>
                        <a target="_blank" href="<?php echo types_render_field('banner_url', array('output' => 'raw')) ?>" class="">
                            <img width="170" height="170" src="<?php echo $imgURL; ?>">
                        </a>
                    </div>
                    <?php endif; ?>
                <?php endwhile; wp_reset_postdata();?>
            </div>
        </div>
    </div>
    <div class="footer">
       <div class="wrapper_content">
            <div id="footer">
                <div class="cocial"><b>Поделиться в социальных сетях:</b>
                    <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                    <span class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus"></span>
                </div>
                <div class="copirate">© insclub.info, 2015 | Все права защищены</div>
                <div style="text-align: right">
                    <!-- MyCounter v.2.0 -->
                    <script type="text/javascript"><!--
                        my_id = 149249;
                        my_width = 88;
                        my_height = 31;
                        my_alt = "MyCounter - счётчик и статистика";
                        //--></script>
                    <script type="text/javascript"
                            src="https://get.mycounter.ua/counter2.0.js">
                    </script>
                    <noscript>
                        <a target="_blank" href="http://mycounter.ua/">
                            <img src="https://get.mycounter.ua/counter.php?id=149249" title="MyCounter - счётчик и статистика" alt="MyCounter - счётчик и статистика" width="88" height="31" border="0" />
                        </a>
                    </noscript>
                    <!--/ MyCounter -->
                </div>
            </div>
       </div>
    </div>
</div>
</body>
</html>