<?php
/**
 * The Header for our theme.
 *
 */
?>
<!DOCTYPE html> 
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>СК<?php wp_title(); ?></title>

    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/basic.css" type="text/css" />
    <!-- bxSlider CSS file -->
    <link href="<?php bloginfo( 'template_url' ); ?>/bxslider/jquery.bxslider.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/carousel/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/carousel/css/jquery.jscrollpane.css" media="all" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Coustard:900' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css' />

    <!-- jQuery library (served from Google) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/bxslider/jquery.bxslider.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/functions.js"></script>
</head>
<body>
    <div class="wrapper">
        <div class="content">
            <div id="header">
                <a href="<?php echo site_url();?>">
                    <img id="slide_1" src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" />
                </a>
            </div>
            <div class="wrapper_content">
                <div id="one_column">
                    <?php
                    $query_banner_left = new WP_Query(
                        array(
                            'post_type' => 'banner',
                            'meta_key' => 'wpcf-banner_column',
                            'meta_value' => 1
                        )
                    );
                    while ( $query_banner_left->have_posts() ) :
                        $query_banner_left->the_post();
                        if (has_post_thumbnail()) :
                            $imgURL = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()) );
                            ?>
                            <div>
                                <a target="_blank" href="<?php echo types_render_field('banner_url', array('output' => 'raw')) ?>" class="">
                                    <img width="170" height="170" src="<?php echo $imgURL; ?>">
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; wp_reset_postdata();?>
                </div>
                <div id="main_column">
                        <div id="banner"></div>
                        <?php
                        $args = array(
                            'menu'            => 'menu',
                            'container'       => 'div',
                            'container_id'    => 'nav',
                          );
                        wp_nav_menu($args); ?>
