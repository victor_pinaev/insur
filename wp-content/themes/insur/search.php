<?php
/**
 * Created by PhpStorm.
 * User: Витя
 * Date: 21.06.2015
 * Time: 20:22
 */
get_header(); ?>
    <div id="content">
        <?php if(have_posts()){?>
            <?php while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>">
                    <header class="entry-header">
                        <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                        <!-- .entry-header --></header>
                    <div class="clearfix entry-content">
                        <?php the_excerpt(); ?>
                        <!-- .entry-content --></div>
                    <!-- #post-<?php the_ID(); ?> --></article>

            <?php endwhile; ?>
        <?php } else {?>
            <h3 style="color: red">По данному запросу результатов не найдено</h3>
        <?php } ?>
    </div>
<?php get_footer(); ?>