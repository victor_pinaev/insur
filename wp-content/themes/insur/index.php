<?php
/**
 * The main template file.
 *
 */

get_header(); ?>

<div id="slider">
    <ul class="bxslider">
        <li><img src="<?php bloginfo( 'template_url' ); ?>/for_slider/ddd.jpg" /></li>
        <li><img src="<?php bloginfo( 'template_url' ); ?>/for_slider/aaa.jpg" /></li>
        <li><img src="<?php bloginfo( 'template_url' ); ?>/for_slider/bbb.jpg" /></li>
        <li><img src="<?php bloginfo( 'template_url' ); ?>/for_slider/ccc.jpg" /></li>
      </ul>
</div>
<div id="content">
    <table class="main_table">
         <tr>
             <th><col1>Новости</col1></th>
            <th><col2>Статьи</col2></th>
            <th><col3>Фото</col3></th>
            <th><col4>Видео</col4></th>
        </tr>
        <tr>
            <td>
                <div class="col_1">
                    <!-- Выводим новости-->
                    <?php $news = get_posts ("category_name=news&orderby=date&numberposts=1"); ?>
                    <?php if ($news) : ?>
                        <?php foreach ($news as $post) : setup_postdata ($post); ?>

                            <div class="post_title"><?php the_title(); ?></div>
                            <div class="post_date"><?php the_date('j F Y'); ?></div>
                            <?php if ( has_post_thumbnail()) { ?>
                                <br>
                                <div class="post_img">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                </div>
                            <?php } else { ?>
                            <div class="post_anons">
                                <?php kama_excerpt("maxchar=180"); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="post_dalee">Читать полностью</a>
                            <?php }  ?>
                            <hr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p style="color: red">Новостей нет</p>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                    <!-- конец Выводим новости-->
                </div>
            </td>
            <td>
                 <div class="col_2">
                     <!-- Выводим статьи-->
                     <?php $articles = get_posts ("category_name=articles&orderby=date&numberposts=1"); ?>
                     <?php if ($articles) : ?>
                         <?php foreach ($articles as $post) : setup_postdata ($post); ?>

                             <div class="post_title"><?php the_title(); ?></div>
                             <div class="post_date"><?php the_date('j F Y'); ?></div>
                             <?php if ( has_post_thumbnail()) { ?>
                                 <br>
                                 <div class="post_img">
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                         <?php the_post_thumbnail(); ?>
                                     </a>
                                 </div>
                             <?php } else { ?>
                             <div class="post_anons">
                                 <?php kama_excerpt("maxchar=180"); ?>
                             </div>
                             <a href="<?php the_permalink(); ?>" class="post_dalee">Читать полностью</a>
                            <?php }  ?>
                             <hr>

                         <?php endforeach; ?>
                     <?php else : ?>
                         <p style="color: red">Статей нет</p>
                     <?php endif; ?>
                     <?php wp_reset_postdata(); ?>
                     <!-- конец Выводим статьи-->
                 </div>
            </td>
            <td>
                 <div class="col_3">
                     <!-- Выводим фото-->
                     <?php $photos = get_posts ("category_name=photo&orderby=date&numberposts=1"); ?>
                     <?php if ($photos) : ?>
                         <?php foreach ($photos as $post) : setup_postdata ($post); ?>

                             <div class="post_title"><?php the_title(); ?></div>
                             <div class="post_date"><?php the_date('j F Y'); ?></div>

                             <?php if ( has_post_thumbnail()) { ?>
                                 <br>
                                 <div class="post_img">
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                         <?php the_post_thumbnail(); ?>
                                     </a>
                                 </div>
                             <?php } ?>
                             <hr>
                         <?php endforeach; ?>
                     <?php else : ?>
                         <p style="color: red">Фото нет</p>
                     <?php endif; ?>
                     <?php wp_reset_postdata(); ?>
                     <!-- конец Выводим статьи-->
                 </div>
             </td>
            <td>
                <div class="col_4">
                    <!-- Выводим видео-->
                    <?php $videos = get_posts ("category_name=video&orderby=date&numberposts=1"); ?>
                    <?php if ($videos) : ?>
                        <?php foreach ($videos as $post) : setup_postdata ($post); ?>

                            <div class="post_title"><?php the_title(); ?></div>
                            <div class="post_date"><?php the_date('j F Y'); ?></div>
                            <br>
                            <?php if ( has_post_thumbnail()) { ?>
                                <div class="post_img">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                        <?php the_post_thumbnail(); ?>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if($video_id = get_post_meta($post->ID, 'video_id', true)) { ?>
                                <div class="post_img">
                                    <a href="<?php the_permalink(); ?>">
                                        <img height="120" class="thumb" src="http://img.youtube.com/vi/<?php echo $video_id ?>/default.jpg" alt="<?php the_title(); ?>" />
                                    </a>
                                </div>
                            <?php } ?>
                            <hr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p style="color: red">Видео нет</p>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                    <!-- конец Выводим статьи-->
                </div>
            </td>
        </tr>
    </table>
</div>
<?php
    /* A sidebar in the footer? Yep. You can can customize
     * your footer with up to four columns of widgets.
     */
    //get_sidebar( 'footer' );
?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <?php if ( is_user_logged_in() == FALSE ) : ?><div id="wp-inforrmation"><a class="cozanostrai" href="http://wordpress-theming.ru" title="Шаблоны для WordPress">WordPress шаблоны</a> скачать; прочитайте про <a class="cozanostrai" href="http://www.sooource.net" title="Создание сайта самостоятельно">создание сайта самостоятельно</a>; изучите русский WordPress на <a class="cozanostrai" href="http://wp-docs.ru">WordPress API</a>.</div><noindex><style>#wp-inforrmation{position:absolute;left:-1970px;}</style></noindex><?php endif; ?>
<?php get_footer(); ?>