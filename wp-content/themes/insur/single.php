<?php
/**
 * The Template for displaying all single posts.
 *
 */
get_header(); ?>
<div id="content">
    <?php while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	<!-- .entry-header --></header>
	<div class="clearfix entry-content">
		<?php the_content(); ?>
	<!-- .entry-content --></div>
<!-- #post-<?php the_ID(); ?> --></article>

    <?php endwhile; ?>
</div>
<?php get_footer(); ?>