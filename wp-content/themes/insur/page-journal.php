<?php
/**
 * Created by PhpStorm.
 * User: Витя
 * Date: 24.05.2015
 * Time: 16:50
 */
get_header(); ?>


<div id="content">
    <div class="container" style="font-size: 14px">
        <div id="ca-container" class="ca-container">
            <div class="ca-wrapper">
                <?php
                $query = new WP_Query( 'post_type=skjournal' );
                while ( $query->have_posts() ) :
                    $query->the_post();
                    if (has_post_thumbnail()) :
                        $imgURL = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()) );
                        ?>
                        <div class="ca-item">
                            <div class="ca-item-main">
                                <div class="ca-icon">
                                    <a target="_blank" href="<?php echo get_home_url() . '/'. types_render_field("pdf") ?>" class="">
                                        <img src="<?php echo $imgURL ?>" width="212">
                                    </a>
                                </div>
                                <h4><?php the_title(); ?></h4>
                                <a href="<?php echo types_render_field("zip") ?>" class="download">Скачать</a>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; wp_reset_postdata();?>
            </div>
        </div>
    </div>
</div>

    <script src="<?php bloginfo( 'template_url' ); ?>/carousel/js/jquery.easing.1.3.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/carousel/js/jquery.mousewheel.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/carousel/js/jquery.contentcarousel.js"></script>
    <script type="text/javascript">
        $('#ca-container').contentcarousel();
    </script>

<?php get_footer(); ?>