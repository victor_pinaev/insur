<?php
/**
 * Created by PhpStorm.
 * User: Витя
 * Date: 17.05.2015
 * Time: 20:23
 */

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

function theme_register_nav_menu() {
    register_nav_menu( 'primary', 'Primary Menu' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 180, 9999 ); // размер миниатюры поста по умолчанию
}

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'category-thumb', 400, 9999 ); // 400 в ширину и без ограничения в высоту
}

/**
 * Обрезка текста (excerpt). Шоткоды вырезаются. Минимальное значение maxchar может быть 22.
 * version - 2.0
 *
 * maxchar     - количество символов.
 * text        - какой текст обрезать (по умолчанию берется excerpt поста, если его нету, то content, если есть тег <!--more-->, то maxchar игнорируется и берется все, что до <!--more--> как есть (с HTML)
 * save_format - Сохранять перенос строк или нет. По умолчанию сохраняется. Если в параметр указать определенные теги, то они НЕ будут вырезаться из обрезанного текста (пример: save_format=<strong><a> )
 * echo        - выводить на экран или возвращать (return) для обработки.
 *
 * @param (строка) $args - аргументы в строке.
 *
 * @return HTML
 */
function kama_excerpt( $args = '' ){
    global $post;

    $default = array( 'maxchar' => 350, 'text' => '', 'save_format' => false, 'more_text' => 'Читать дальше...', 'echo' => true, );

    parse_str( $args, $_args );
    $args = array_merge( $default, $_args );
    extract( $args );

    if( ! $text ){
        $text = $post->post_excerpt ? $post->post_excerpt : $post->post_content;

        $text = preg_replace ("~\[/?.*?\]~", '', $text ); // убираем шоткоды, например:[singlepic id=3]

        // для тега <!--more-->
        if( ! $post->post_excerpt && strpos( $post->post_content, '<!--more-->') ){
            preg_match ('~(.*)<!--more-->~s', $text, $match );
            $text = trim( $match[1] );
            $text = str_replace("\r", '', $text );
            $text = preg_replace( "~\n\n+~s", "</p><p>", $text );
            $text = '<p>'. str_replace( "\n", '<br />', $text ) .' <a href="'. get_permalink( $post->ID ) .'#more-'. $post->ID .'">'. $more_text .'</a></p>';

            if( $echo ) return print $text;

            return $text;
        }
        elseif( ! $post->post_excerpt )
            $text = strip_tags( $text, $save_format );
    }

    // Обрезаем
    if ( mb_strlen( $text ) > $maxchar ){
        $text = mb_substr( $text, 0, $maxchar );
        $text = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ...', $text ); // убираем последнее слово, оно 99% неполное
    }

    // Сохраняем переносы строк. Упрощенный аналог wpautop()
    if( $save_format ){
        $text = str_replace("\r", '', $text );
        $text = preg_replace("~\n\n+~", "</p><p>", $text );
        $text = "<p>". str_replace ("\n", "<br />", trim( $text ) ) ."</p>";
    }

    //$out = preg_replace('@\*[a-z0-9-_]{0,15}\*@', '', $out); // удалить *some_name-1* - фильтр сммайлов

    if( $echo ) return print $text;

    return $text;
}
